use crate::widgets::main_widget::MainWidget;

pub struct MaximinApplication {}

impl MaximinApplication {
  pub fn new() -> Self {
    Self {}
  }

  pub fn run(&self) {
    env_logger::init();

    info!("Application starting...");
    relm::run::<MainWidget>(());
    info!("Application shutdown...");
  }
}

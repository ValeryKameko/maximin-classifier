use na::Point2;

#[derive(Debug)]
pub struct Bounds {
  pub minx: f32,
  pub maxx: f32,
  pub miny: f32,
  pub maxy: f32,
}

impl Bounds {
  pub fn empty() -> Self {
    Self::from_size(0.0, 0.0)
  }

  pub fn from_size(width: f32, height: f32) -> Self {
    Self::new(0.0, 0.0, width, height)
  }

  pub fn new(minx: f32, miny: f32, maxx: f32, maxy: f32) -> Self {
    Self {
      minx: minx,
      miny: miny, 
      maxx: maxx, 
      maxy: maxy
    }
  }

  pub fn update(&mut self, point: &Point2<f32>) {
    self.minx = self.minx.min(point.x);
    self.maxx = self.maxx.max(point.x);
    self.miny = self.miny.min(point.y);
    self.maxy = self.maxy.max(point.y);
  }
}

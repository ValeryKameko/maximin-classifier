use na::Point2;
use ordered_float::OrderedFloat;

#[derive(Clone, Debug)]
pub struct Cluster {
  pub center: Point2<f32>,
  pub points: Vec<Point2<f32>>,
}

impl Cluster {
  pub fn new(center: Point2<f32>, points: Vec<Point2<f32>>) -> Self {
    Self {
      center: center,
      points: points,
    }
  }

  pub fn distance_to(&self, point: &Point2<f32>) -> f32 {
    na::distance(point, &self.center)
  }

  pub fn find_new_center_index(&self) -> Option<(usize, f32)> {
    if self.points.len() == 0 {
      return None;
    }
    self
      .points
      .iter()
      .map(|point| self.distance_to(point))
      .enumerate()
      .max_by_key(|(_, distance)| OrderedFloat(distance.clone()))
  }

  pub fn extract_by(&mut self, new_center: &Point2<f32>) -> Vec<Point2<f32>> {
    let (new_points, new_cluster_points): (_, Vec<_>) = self
      .points
      .iter()
      .partition(|point| self.distance_to(point) < na::distance(new_center, point));
    self.points = new_points;
    return new_cluster_points
  }

  pub fn new_with_center(center: &Point2<f32>, mut points: Vec<Point2<f32>>) -> Self {
    let center_index = points
      .iter()
      .enumerate()
      .min_by_key(|(_, point)| OrderedFloat(na::distance(center, point)))
      .map(|tuple| tuple.0)
      .unwrap();
    let new_center = points.remove(center_index);
    Self {
      center: new_center,
      points: points,
    }
  }
}

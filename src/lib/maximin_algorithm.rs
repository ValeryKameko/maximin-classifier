use super::maximin_state::MaximinState;
use crate::models::cluster::Cluster;
use na::Point2;
use std::cmp;
use ordered_float::OrderedFloat;

pub struct MaximinAlgorithm {}

impl MaximinAlgorithm {
  pub fn extract_new_cluster(state: &mut MaximinState) -> bool {
    let new_center_index = Self::find_new_center_index(state);
    if let Some((cluster_index, point_index)) = new_center_index {
      let half_avg_distance = Self::calc_avg_distance(state) / 2.0;

      let cluster = &state.clusters[cluster_index];
      let new_center = cluster.points[point_index];
      if half_avg_distance > cluster.distance_to(&new_center) {
        return false;
      }
      
      let mut new_cluster_points = vec![];
      for cluster in &mut state.clusters {
        let mut points = cluster.extract_by(&new_center);
        new_cluster_points.append(&mut points);
      }
      let new_cluster = Cluster::new_with_center(&new_center, new_cluster_points);
      state.clusters.push(new_cluster);
      return true;
    } else {
      return false;
    }
  }

  fn find_new_center_index(state: &MaximinState) -> Option<(usize, usize)> {
    let center_indices = state
      .clusters
      .iter()
      .map(|cluster| cluster.find_new_center_index());

    let mut index: Option<(usize, usize, f32)> = None;
    for (cluster_index, new_center_index) in center_indices.enumerate() {
      if let Some((point_index, distance)) = new_center_index {
        let new_index = (cluster_index, point_index, distance);
        if let Some(old_index) = index {
          index = [old_index, new_index]
            .iter()
            .max_by_key(|index| OrderedFloat(index.2))
            .map(|index| index.clone());
        } else {
          index = Some(new_index)
        }
      }
    }

    return index.map(|index| (index.0, index.1));
  }

  fn calc_avg_distance(state: &mut MaximinState) -> f32 {
    let clusters = &state.clusters;
    let mut sum_distance: f32 = 0.0;
    let mut count_distance: f32 = 0.0;
    for (i, first_cluster) in clusters.iter().enumerate() {
      for second_cluster in clusters[(i + 1)..].iter() {
        count_distance += 1.0;
        sum_distance += na::distance(&first_cluster.center, &second_cluster.center);
      }
    }
    return sum_distance / count_distance;
  }
}

use rand::Rng;
use rand::distributions::{Distribution, Uniform};
use na::{Point2};

use super::super::point_generator::PointGenerator;

pub struct UniformPointGenerator {
  distribution: Uniform<f32>,
}

impl UniformPointGenerator {
  pub fn new(min: f32, max: f32) -> Self {
    return Self {
      distribution: Uniform::new(min, max),
    }
  }
}

impl PointGenerator for UniformPointGenerator {
  fn generate(&self) -> Point2<f32> {
    let mut rng = rand::thread_rng();
    let x = self.distribution.sample(&mut rng);
    let y = self.distribution.sample(&mut rng);
    return Point2::<f32>::new(x, y)
  }
}


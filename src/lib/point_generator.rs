use na::{Point2};

pub trait PointGenerator {
  fn generate(&self) -> Point2<f32>;
}
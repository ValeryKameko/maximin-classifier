use crate::models::cluster::Cluster;
use rand::Rng;
use na::Point2;

use super::point_generator::PointGenerator;

#[derive(Clone, Debug)]
pub struct MaximinState {
  pub clusters: Vec<Cluster>,
}

impl MaximinState {
  pub fn new() -> Self {
    return Self {
      clusters: vec![],
    }
  }

  pub fn generate(count: i32, generator: &dyn PointGenerator) -> Self {
    assert!(count > 0);
    let mut points = Vec::new();
    for _ in 1..count {
      points.push(generator.generate());
    }
    let center = Self::choose_center(&mut points);
    return Self {
      clusters: vec![Cluster::new(center, points)]
    }
  }

  fn choose_center(points: &mut Vec<Point2<f32>>) -> Point2<f32> {
    assert!(points.len() > 1);
    let mut rng = rand::thread_rng();
    let center_pos = rng.gen_range(0, points.len() - 1);
    let center = points.remove(center_pos);
    return points[center_pos];
  }
}
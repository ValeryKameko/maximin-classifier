use color::{Deg, Hsv, Rgb, ToRgb};
use gdk::{EventMask, RGBA};
use gtk::prelude::*;
use gtk::GtkWindowExt;
use gtk::{DrawingArea, Orientation::*};
use na::Point2;
use relm::{Component, DrawHandler, Relm, Update, Widget};
use relm_derive::{widget, Msg};
use std::f32;
use cairo::Context;
use delaunay2d::{Delaunay2D};

use crate::models::{bounds::Bounds, cluster::Cluster};

#[derive(Msg)]
pub enum Msg {
  UpdateDrawBuffer,
  UpdateClusters(Vec<Cluster>),
}

pub struct Model {
  draw_handler: DrawHandler<DrawingArea>,
  relm: relm::Relm<ClustersVisualizer>,
  clusters: Vec<Cluster>,
  bounds: Bounds,
}

impl ClustersVisualizer {
  const MIN_GAP: f32 = 10.0;

  fn calculate_bounds(clusters: &Vec<Cluster>) -> Bounds {
    let mut bounds = Bounds::new(f32::INFINITY, f32::INFINITY, -f32::INFINITY, -f32::INFINITY);
    for cluster in clusters {
      bounds.update(&cluster.center);
      for &point in &cluster.points {
        bounds.update(&point);
      }
    }
    Self::normailze_range(&mut bounds.minx, &mut bounds.maxx);
    Self::normailze_range(&mut bounds.miny, &mut bounds.maxy);
    bounds
  }

  fn normailze_range(min: &mut f32, max: &mut f32) {
    let diff = *max - *min;
    if diff < Self::MIN_GAP {
      *min -= diff / 2.0;
      *max += diff / 2.0;
    }
  }

  fn normalize_point(&self, point: &Point2<f32>, new_bounds: &Bounds) -> Point2<f32> {
    let bounds = &self.model.bounds;
    let mut new_point = point.clone();
    new_point.x =
      (point.x - bounds.minx) / (bounds.maxx - bounds.minx) * (new_bounds.maxx - new_bounds.minx);
    new_point.y =
      (point.y - bounds.miny) / (bounds.maxy - bounds.miny) * (new_bounds.maxy - new_bounds.miny);
    return new_point;
  }

  fn draw_cluster_center(context: &Context, color: &Rgb<f64>, point: &Point2<f32>) {
    context.set_line_width(2.0);
    context.set_source_rgb(color.r, color.g, color.b);
    context.arc(
      point.x as f64,
      point.y as f64,
      6.0,
      0.0,
      2.0 * std::f64::consts::PI,
    );
    context.fill_preserve();
    context.set_source_rgb(0.0, 0.0, 0.0);
    context.stroke();
  }

  fn draw_data_point(context: &Context, color: &Rgb<f64>, point: &Point2<f32>) {
    context.set_source_rgb(color.r, color.g, color.b);
    context.arc(
      point.x as f64,
      point.y as f64,
      2.0,
      0.0,
      2.0 * std::f64::consts::PI,
    );
    context.fill();
  }

  fn draw_borders(&self, context: &Context, bounds: &Bounds) {
    let center = (0.0, 0.0);
    let size = 9999.0;
    let mut delaunay = Delaunay2D::new(center, size);
    for cluster in &self.model.clusters {
      delaunay.add_point((cluster.center.x as f64, cluster.center.y as f64));
    }
    let (points, regions) = delaunay.export_voronoi_regions();

    for region in regions {
      context.set_source_rgb(0.0, 0.0, 0.0);

      context.new_path();
      for point_index in region {
        let point = Point2::new(points[point_index].0 as f32, points[point_index].1 as f32);
        let norm_point = self.normalize_point(&point, bounds);
        context.line_to(norm_point.x as f64, norm_point.y as f64);
      }
      context.close_path();

      context.stroke();
    }
  }

  fn draw_clusters(&self, context: &Context, bounds: &Bounds) {
    let clusters = &self.model.clusters;
    self.draw_borders(context, bounds);
    for (i, cluster) in clusters.iter().enumerate() {
      let hue = (i as f32) / (clusters.len() as f32);
      let color = Hsv::new(Deg(hue * 360.0), 1.0, 1.0).to_rgb();

      let point = self.normalize_point(&cluster.center, bounds);
      Self::draw_cluster_center(context, &color, &point);
      for point in &cluster.points {
        let norm_point = self.normalize_point(&point, bounds);
        Self::draw_data_point(context, &color, &norm_point);
      }
    }
  }
}

#[widget]
impl Widget for ClustersVisualizer {
  fn init_view(&mut self) {
    self.model.draw_handler.init(&self.drawing_area);
    self.model.relm.stream().emit(Msg::UpdateDrawBuffer);
  }

  fn update(&mut self, event: Msg) {
    use Msg::*;

    match event {
      UpdateDrawBuffer => {
        let context = self.model.draw_handler.get_context();
        let (width, height) = (
          self.drawing_area.get_allocated_width(),
          self.drawing_area.get_allocated_height(),
        );
        let bounds = Bounds::from_size(width as f32, height as f32);
        context.set_source_rgb(1.0, 1.0, 1.0);
        context.paint();

        self.draw_clusters(&context, &bounds);
      }
      UpdateClusters(clusters) => {
        self.model.clusters = clusters;
        self.model.bounds = Self::calculate_bounds(&self.model.clusters);
        self.model.relm.stream().emit(UpdateDrawBuffer);
      }
    }
  }

  fn model(relm: &relm::Relm<Self>, _: ()) -> Model {
    Model {
      relm: relm.clone(),
      draw_handler: DrawHandler::new().expect("draw handler"),
      clusters: vec![],
      bounds: Bounds::empty(),
    }
  }

  view! {
    gtk::Box {
      #[name="drawing_area"]
      gtk::DrawingArea {
        child: {
          expand: true,
        },
        configure_event(_, _) => (Msg::UpdateDrawBuffer, true),
      }
    }
  }
}

use gtk::prelude::*;
use relm::{Component, Relm, Update, Widget};
use relm_derive::{widget, Msg};

use crate::lib::{
  maximin_algorithm::MaximinAlgorithm, maximin_state::MaximinState,
  point_generators::uniform_point_generator::UniformPointGenerator,
};
use crate::widgets::{clusters_visualizer::ClustersVisualizer, *};

#[derive(Eq, PartialEq, Copy, Clone)]
pub enum State {
  Generating,
  AlgorithmIterating,
  AlgorithmEnd,
}

#[derive(Msg)]
pub enum Msg {
  InputChanged,
  ChangeState(State),
  Next,
  Restart,
  Generate,
  Quit,
}

pub struct Model {
  count: i32,
  state: State,
  maximin_state: MaximinState,
  relm: relm::Relm<MainWidget>,
}

#[widget]
impl Widget for MainWidget {
  fn init_view(&mut self) {
    self.window.resize(800, 600);
    self.window.set_gravity(gdk::Gravity::Center);
    self.window.set_size_request(1366, 768);

    self
      .model
      .relm
      .stream()
      .emit(Msg::ChangeState(State::Generating));
    self.model.relm.stream().emit(Msg::InputChanged);
  }

  fn update(&mut self, event: Msg) {
    use Msg::*;

    match event {
      Quit => gtk::main_quit(),
      Generate => {
        let generator = UniformPointGenerator::new(-50.0, 50.0);
        self.model.maximin_state = MaximinState::generate(self.model.count, &generator);
        self
          .clusters_visualizer
          .emit(clusters_visualizer::Msg::UpdateClusters(
            self.model.maximin_state.clusters.clone(),
          ));
        self
          .model
          .relm
          .stream()
          .emit(ChangeState(State::AlgorithmIterating));
        self.next.set_sensitive(true);
      }
      Next => {
        let result = MaximinAlgorithm::extract_new_cluster(&mut self.model.maximin_state);
        if !result {
          let dialog = gtk::MessageDialog::new(
            Some(&self.window),
            gtk::DialogFlags::empty(),
            gtk::MessageType::Info,
            gtk::ButtonsType::Ok,
            "Algorithm end",
          );
          dialog.run();
          dialog.close();
          self.next.set_sensitive(false);
        }
        self
          .clusters_visualizer
          .emit(clusters_visualizer::Msg::UpdateClusters(
            self.model.maximin_state.clusters.clone(),
          ));
      }
      Restart => {
        self.model.maximin_state = MaximinState::new();
        self
          .clusters_visualizer
          .emit(clusters_visualizer::Msg::UpdateClusters(
            self.model.maximin_state.clusters.clone(),
          ));
        self
          .model
          .relm
          .stream()
          .emit(ChangeState(State::Generating));
      }
      InputChanged => {
        let text = self.count_entry.get_buffer().get_text();
        match text.parse::<i32>() {
          Ok(count) => {
            self.model.count = count;
            self
              .generate_button
              .set_sensitive(count > 0 && count <= 100000);
          }
          Err(_) => self.generate_button.set_sensitive(false),
        }
      }
      ChangeState(state) => {
        self.model.state = state;
        self.generate.set_visible(state == State::Generating);
        self.restart.set_visible(state != State::Generating);
        self.next.set_visible(state == State::AlgorithmIterating);
      }
    }
  }

  fn model(relm: &relm::Relm<Self>, _: ()) -> Model {
    Model {
      count: 0,
      state: State::Generating,
      relm: relm.clone(),
      maximin_state: MaximinState::new(),
    }
  }

  view! {
    #[name="window"]
    gtk::Window {
      title: "Maximin application",
      gtk::Box {
        orientation: gtk::Orientation::Horizontal,
        #[name="clusters_visualizer"]
        ClustersVisualizer {
          child: {
            fill: true,
            expand: true,
          }
        },
        #[name="side_view"]
        gtk::Box {
          orientation: gtk::Orientation::Vertical,
          #[name="generate"]
          gtk::Box {
            orientation: gtk::Orientation::Vertical,
            #[name="count_entry"]
            gtk::Entry {
              changed => Msg::InputChanged,
            },
            #[name="generate_button"]
            gtk::Button {
              label: "Generate",
              clicked => Msg::Generate,
            },
          },
          #[name="next"]
          gtk::Button {
            label: "Next",
            clicked => Msg::Next,
          },
          #[name="restart"]
          gtk::Button {
            label: "Restart",
            clicked => Msg::Restart,
          }
        },
      },
      delete_event(_, _) => (Msg::Quit, Inhibit(false)),
    }
  }
}

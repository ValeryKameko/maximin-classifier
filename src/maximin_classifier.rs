#[macro_use] extern crate log;
extern crate gtk;
extern crate nalgebra as na;

mod maximin_application;
mod models;
mod lib;
mod widgets;

use maximin_application::MaximinApplication;

fn main() {
  let application = MaximinApplication::new();
  application.run();
}
